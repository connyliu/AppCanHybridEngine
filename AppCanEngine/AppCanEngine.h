/**
 *
 *	@file   	: AppCanEngine.h  in AppCanEngine
 *
 *	@author 	: CeriNo 
 * 
 *	@date   	: Created on 16/1/18.
 *
 *	@copyright 	: 2015 The AppCan Open Source Project.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#import <Foundation/Foundation.h>


@interface WidgetOneDelegate: NSObject
@end
@interface AppCanEngine : WidgetOneDelegate<UIApplicationDelegate>


/**
 *  rootViewController 是承载rootWebView的controller
 *
 *  @return rootViewController
 */
- (UIViewController *)ace_rootViewController;
/**
 *  startView是rootWebView加载完成之前在ViewController中被展示的View;
 *
 *  @return startView;
 */
- (UIView *)ace_startView;

/**
 *  rootWebView加载完成时会执行此方法
 */
- (void)rootPageDidFinishLoading NS_REQUIRES_SUPER;

//以下都是系统的回调方法
//必须要调用super的方法;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification NS_REQUIRES_SUPER;
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url NS_REQUIRES_SUPER;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation NS_REQUIRES_SUPER;
- (void)applicationWillResignActive:(UIApplication *)application NS_REQUIRES_SUPER;
- (void)applicationDidBecomeActive:(UIApplication *)application NS_REQUIRES_SUPER;
- (void)applicationDidEnterBackground:(UIApplication *)application NS_REQUIRES_SUPER;
- (void)applicationWillEnterForeground:(UIApplication *)application NS_REQUIRES_SUPER;
- (void)applicationWillTerminate:(UIApplication *)application NS_REQUIRES_SUPER;
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application  NS_REQUIRES_SUPER;
- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler NS_REQUIRES_SUPER;
- (void)invokeAppDelegateMethodApplication:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler NS_REQUIRES_SUPER;

@end
