#AppCanHybridEngine

version:0.0.3 `beta`

##使用方法：

###1.将AppCanEngine目录以`group`方式引入工程


###2.新建名为`widget`文件夹并将`AppCan的网页文件`放入其中，然后以`folder references`方式将此文件夹引入工程
###3.将所需要的AppCan插件以及配置好的`plugin.xml`以`group`方式引入工程(如不需要插件，此步可跳过)
###4.添加如下系统库至工程中
* `libxml2.tbd`
* `libz.tbd`
* `libstdc++.tbd`
* `libsqlite3.tbd`
* `ImageIO.framework`
* `SystemConfiguration.framework`
* `CFNetwork.framework`
* `AudioToolbox.framework`
* `MobileCoreServices.framework`
* `MapKit.framework`

###5.AppDelegate需继承自AppCan的引擎类`AppCanEngine`,并在相应的UIApplicationDelegate方法中调用super的相关方法
###6.如何打开/关闭网页，参考Demo

***
####注:beta期间AppCanEngine目录下的库文件均包含真机架构和模拟器架构。待release时会替换成只包含真机架构的。
	
***

##更新历史
* 0.0.3
	* update:2016-01-20 
	* 继续抽出如下第三方库的源码:MBProgressHUD,SDJSON,NSData+Base64

* 0.0.2
	* update:2016-01-19 
	* 抽出部分公用的第三方库源码，解决冲突问题
	* `AppCanEngine/uex3rdSourceCode/`下为抽出的部分第三方库源码，请按需删去冲突的部分。
	* **部分源码文件可能需要添加`-fno-objc-arc`或者`-fobjc-arc`的compile flag**（参考DEMO工程）
* 0.0.1
	* update:2016-01-18
	* 初始化
