//
//  AppDelegate.m
//  seperatedEngineDemo
//
//  Created by CeriNo on 16/1/18.
//  Copyright © 2016年 AppCan. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
@interface AppDelegate ()

@end

NSString *const kAppCanRootWebViewDidFinishLoading = @"kAppCanRootWebViewDidFinishLoading";

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [super application:application didFinishLaunchingWithOptions:launchOptions];
    self.window.rootViewController = [[ViewController alloc]init];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)rootPageDidFinishLoading{
    [super rootPageDidFinishLoading];
    [[NSNotificationCenter defaultCenter]postNotificationName:kAppCanRootWebViewDidFinishLoading object:nil];
    
}
@end
